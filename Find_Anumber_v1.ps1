﻿$Anum = "+79031584783"
$Date1 = 20220426
$Date2 = 20220427
$GUCID = "msccsrv40435278861445351"
$Query = Invoke-Sqlcmd -query "SELECT TOP (10) * FROM IPN_STAT.dbo.ACDR where CTI_ANumber like '%$Anum%' and CTI_GUCID like '%$GUCID%' and dt_add between '$Date1' and '$Date2' order by CTI_CallArrivedDT asc" -ServerInstance "MS-CCDBCLS001"
$QueryFilter = $Query | Select-Object -Property ACD_RPNodeID,CTI_GUCID,CTI_WorkplaceID,AP_AgentID,
@{Label='ACD_ACDNodeID'; expression={($_.ACD_ACDNodeID).Split(".")[0]}},
@{Label='VEXT_CALLID'; expression={($_.VEXT_CALLID).Split(".")[0]}}
$Destination = ".\"
$n = 1
foreach  ($name in $QueryFilter){
if ((get-date).ToString('yyyyMMdd') -eq $Date1) {
New-Item -Path $Destination\$($Anum)\$($name.CTI_GUCID)\ -ItemType Directory -Force
Copy-Item -Path "\\$($name.ACD_RPNodeID)\c$\IS3\Logs\RPTelserv_$($Date1).log" -Destination $Destination\$($Anum)\$($name.CTI_GUCID)\$($n++;"$($n-1)$_")_$($name.ACD_RPNodeID)_RP_RPTelserv.log -Force
Copy-Item -Path "\\$($name.ACD_RPNodeID)\c$\IS3\Logs\TA_$($Date1).log" -Destination $Destination\$($Anum)\$($name.CTI_GUCID)\$($n++;"$($n-1)$_")_$($name.ACD_RPNodeID)_RP_TA.log -Force
Copy-Item -Path "\\$($name.ACD_ACDNodeID)\c$\IPN\Logs\acd\ACDCON_$($Date1)_000000.log" -Destination $Destination\$($Anum)\$($name.CTI_GUCID)\$($n++;"$($n-1)$_")_$($name.ACD_ACDNodeID)_ACDCON.log -Force
Copy-Item -Path "\\$($name.ACD_ACDNodeID)\c$\IPN\Logs\acd\ACD_$($Date1)_000000.log" -Destination $Destination\$($Anum)\$($name.CTI_GUCID)\$($n++;"$($n-1)$_")_$($name.ACD_ACDNodeID)_ACD.log -Force
Copy-Item -Path "\\$($name.VEXT_CALLID)\c$\IS3\Logs\RPTelserv_$($Date1).log" -Destination $Destination\$($Anum)\$($name.CTI_GUCID)\$($n++;"$($n-1)$_")_$($name.VEXT_CALLID)_Vext_RPTelserv.log -Force
Copy-Item -Path "\\$($name.VEXT_CALLID)\c$\IS3\Logs\TA_$($Date1).log" -Destination $Destination\$($Anum)\$($name.CTI_GUCID)\$($n++;"$($n-1)$_")_$($name.VEXT_CALLID)_Vext_TA.log -Force
}
elseif ((get-date).AddDays(-1).ToString('yyyyMMdd') -eq $Date1) {
New-Item -Path $Destination\$($Anum)\$($name.CTI_GUCID)\ -ItemType Directory -Force
Copy-Item -Path "\\$($name.ACD_RPNodeID)\c$\IS3\Logs\RPTelserv_$($Date1).log" -Destination $Destination\$($Anum)\$($name.CTI_GUCID)\$($n++;"$($n-1)$_")_$($name.ACD_RPNodeID)_RP_RPTelserv.log -Force
Copy-Item -Path "\\$($name.ACD_RPNodeID)\c$\IS3\Logs\TA_$($Date1).log" -Destination $Destination\$($Anum)\$($name.CTI_GUCID)\$($n++;"$($n-1)$_")_$($name.ACD_RPNodeID)_RP_TA.log -Force
Copy-Item -Path "\\$($name.ACD_ACDNodeID)\c$\IPN\Logs\acd\ACDCON_$($Date1)_000000.log" -Destination $Destination\$($Anum)\$($name.CTI_GUCID)\$($n++;"$($n-1)$_")_$($name.ACD_ACDNodeID)_ACDCON.log -Force
Copy-Item -Path "\\$($name.ACD_ACDNodeID)\c$\IPN\Logs\acd\ACD_$($Date1)_000000.log" -Destination $Destination\$($Anum)\$($name.CTI_GUCID)\$($n++;"$($n-1)$_")_$($name.ACD_ACDNodeID)_ACD.log -Force
Copy-Item -Path "\\$($name.VEXT_CALLID)\c$\IS3\Logs\RPTelserv_$($Date1).log" -Destination $Destination\$($Anum)\$($name.CTI_GUCID)\$($n++;"$($n-1)$_")_$($name.VEXT_CALLID)_Vext_RPTelserv.log -Force
Copy-Item -Path "\\$($name.VEXT_CALLID)\c$\IS3\Logs\TA_$($Date1).log" -Destination $Destination\$($Anum)\$($name.CTI_GUCID)\$($n++;"$($n-1)$_")_$($name.VEXT_CALLID)_Vext_TA.log -Force
}
else{
New-Item -Path $Destination\$($Anum)\$($name.CTI_GUCID)\ -ItemType Directory -Force
Copy-Item -Path "\\$($name.ACD_RPNodeID)\c$\IS3\Logs\Archive\RPTelserv_$($Date1+2).zip" -Destination $Destination\$($Anum)\$($name.CTI_GUCID)\$($n++;"$($n-1)$_")_$($name.ACD_RPNodeID)_RP_RPTelserv.zip -Force
Copy-Item -Path "\\$($name.ACD_RPNodeID)\c$\IS3\Logs\Archive\transitagent_$($Date1+2).zip" -Destination $Destination\$($Anum)\$($name.CTI_GUCID)\$($n++;"$($n-1)$_")_$($name.ACD_RPNodeID)_RP_TA.zip -Force
Copy-Item -Path "\\$($name.ACD_ACDNodeID)\c$\IPN\Logs\acd\ACDCON_$($Date1)_000000.log" -Destination $Destination\$($Anum)\$($name.CTI_GUCID)\$($n++;"$($n-1)$_")_$($name.ACD_ACDNodeID)_ACDCON.log -Force
Copy-Item -Path "\\$($name.ACD_ACDNodeID)\c$\IPN\Logs\acd\ACD_$($Date1)_000000.log" -Destination $Destination\$($Anum)\$($name.CTI_GUCID)\$($n++;"$($n-1)$_")_$($name.ACD_ACDNodeID)_ACD.log -Force
Copy-Item -Path "\\$($name.VEXT_CALLID)\c$\IS3\Logs\Archive\RPTelserv_$($Date1+2).zip" -Destination $Destination\$($Anum)\$($name.CTI_GUCID)\$($n++;"$($n-1)$_")_$($name.VEXT_CALLID)_Vext_RPTelserv.zip -Force
Copy-Item -Path "\\$($name.VEXT_CALLID)\c$\IS3\Logs\Archive\transitagent_$($Date1+2).zip" -Destination $Destination\$($Anum)\$($name.CTI_GUCID)\$($n++;"$($n-1)$_")_$($name.VEXT_CALLID)_Vext_TA.zip -Force
  }
}