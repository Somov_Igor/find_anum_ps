﻿cls
#Присваиваем переменные: Anum - слитно и желательно без +7; Date - строго в формате yyyy-MM-dd; GUCID - можно не указывать если не известен; OutFile - указываем каталог, где создавать файл с данными
$Anum = "9637885823"
$Date = "2022-07-06"
$GUCID = ""
$OutFile = ".\out_$((get-date).ToString('HH-mm_dd-MM-yyyy')).txt"
#Делаем запрос в DB is3_isup_calls
Write-Host "Делаем запрос в DB is3_isup_calls_$DateDB ..."
$DateDB = $Date.Split("-") -join ""
$Query1 = Invoke-Sqlcmd -query "SELECT * FROM [rostov_is3].[dbo].[is3_isup_calls_$DateDB] where origination like '%$Anum%'"  -ServerInstance "MS-VRP816" -ErrorAction SilentlyContinue
    #Если ничего не нашлось, делаем запрос в DB ACDR
    if($Query1.Count -eq 0){
        Write-Host "Звонок не найден в DB is3_isup_calls_$DateDB ..."
        Write-Host "Делаем запрос в DB ACDR ..."
        $Query = Invoke-Sqlcmd -query "SELECT * FROM IPN_STAT.dbo.ACDR where CTI_ANumber like '%$Anum%' and CTI_GUCID like '%$GUCID%' and CONVERT(DATETIME, FLOOR(CONVERT(FLOAT, dt_add))) = '$Date' order by CTI_CallArrivedDT asc" -ServerInstance "MS-CCDBCLS001" -ErrorAction SilentlyContinue
        $QueryFilterACDR = $Query | Select-Object -Property dt_add,ACD_RPNodeID,CTI_GUCID,CTI_WorkplaceID,AP_AgentID,
        @{Label='ACD_ACDNodeID'; expression={($_.ACD_ACDNodeID).Split(".")[0]}},
        @{Label='VEXT_CALLID'; expression={($_.VEXT_CALLID).Split(".")[0]}}
        Write-Host "Дальше программа не проработана, ниже должен быть вывод из БД"
        $QueryFilterACDR
        }
foreach($QueryStr in $Query1){
    #Выводим ответ из DB is3_isup_calls
    Write-Host "Полученные данные host -"$QueryStr.host"; Anum -"$QueryStr.origination"; Bnum -"$QueryStr.destination"; GUCID -"$QueryStr.GUCID
    $newArray1 = @()
    $newArray2 = @()
    #Ищем строки с Anum и scriptID в логах CoreNew* на сервере
    Write-Host "Ищем scriptID в логах CoreNew* на сервере"$QueryStr.host"..."
    $FindScriptIDs = Get-ChildItem -Path "\\$($QueryStr.host)\c$\IS3\Logs\CoreNew*" | Select-String "$Anum" | Select-String " scriptID "
    #Разбираем строки и выбираем только scriptID
        foreach($FindScriptID in $FindScriptIDs){
                $1 = $FindScriptID.Line.Split(";") | Select-String " ScriptID "
                    foreach($2 in $1){
                        $newArray1 += $2.Line.Split(" ")[3]
                        }
                }
    
    #Фильтруем массив данных и выводим только уникальные
    $ScriptIDs = $newArray1 | Select-Object -Unique
    Write-Host "Найдены следующие данные:"
    $ScriptIDs
    #Ищем строки scriptID в логах RPTelserv*, разбираем и оставляем только 1й блок callID
    Write-Host "Ищем callID в логах RPTelserv* на сервере"$QueryStr.host"..."
    foreach($ScriptID in $ScriptIDs){
        $5 = Get-ChildItem -Path "\\$($QueryStr.host)\c$\IS3\Logs\RPTelserv*" | Select-String "$ScriptID"
            #разбираем строку оставляем только []-[]
            foreach($6 in $5){
                $7 = $6.Line.Split(" ")[7]
                    #разбираем строку оставляем только [] callID
                    foreach($8 in $7){
                        $newArray2 += $8.Split("-")[0]          
                        }
                }
        }
    #Фильтруем массив данных и выводим только уникальные
    $CutCalls = $newArray2 | Select-Object -Unique
    Write-Host "Найдены следующие данные:"
    $CutCalls
    #Фильтруем RPTelserv* по callID
    Write-Host "Забираем из лога RPTelserv* все строки с"$CutCalls 
    $output = foreach($CutCall in $CutCalls){
                    Write-Output "**************************************************************************************************"
                    Write-Output "Полученные данные:" $QueryStr.host $QueryStr.origination $QueryStr.destination $QueryStr.GUCID
                    Write-Output "find "$CutCall
                    Write-Output "**************************************************************************************************"
                    (Get-ChildItem -Path "\\$($QueryStr.host)\c$\IS3\Logs\RPTelserv*" | Select-String -SimpleMatch "$CutCall").Line
                    }
    #Выводим данные в файл
    Write-Output "Выводим данные в файл"$OutFile "..."
    $output | Out-File $OutFile -Append
    }